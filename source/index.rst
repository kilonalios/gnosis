.. having_fun_with documentation master file, created by
   sphinx-quickstart on Wed Nov 25 13:32:52 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to having_fun_with's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   python/introduction



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
